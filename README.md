# UNGS-Votacion

La UNGS va a ser sede de votación para las próximas elecciones de noviembre, y
el gobierno nacional le pidió a la Universidad que gestione la asignación de turnos.


Se brindan las siguientes especificaciones:
Para respetar los protocolos actuales, deberán existir diferentes tipos de mesas y
distintos horarios de turnos:

-  Mesas para mayores, hombres y mujeres a partir de 65 años, las cuales
tienen un cupo de 10 personas en cada franja horaria.

-  Mesas para personas con enfermedades preexistentes con cupo de 20 por
franja horaria.

-  Mesas para personas que tienen que trabajar ese día (con certificado de
trabajo). Tiene una única franja horaria de 8hs a 12hs sin cupo.

-  Mesas para personas en ninguno de estos grupos con cupo de hasta 30
personas por turno.

- Las mesas pueden tener una sola de las características mencionadas.
- Durante el proceso de votación se podrán abrir mesas nuevas en el caso de
que se agoten los turnos.
- El presidente de mesa asignado no tiene posibilidad de ausentarse a la
misma y consumirá uno de los turnos de su mesa.
Respecto a la asignación de turnos:

-  El periodo de votación se divide en 10 franjas horarias, comenzando a las 8
hs. Franjas horarias: 8 a 9, 9 a 10, 10 a 11, ... ,  17 a 18.

-  De cada persona se conoce su dni, nombre, si es mayor, si tiene alguna
enfermedad preexistente o si trabaja el día de la votación. También se conoce
si es presidente de mesa.

-  Cada persona puede votar solamente en una mesa que se ajuste a su
situación. Por ejemplo: En el caso de los presidentes de mesa debe votar en
la mesa en la que es presidente independientemente del tipo de mesa que
sea:
○ Si es trabajador, vota en una mesa para Trabajadores. Ignorando si
es mayor o si tiene alguna enfermedad preexistente.
○ Si es mayor de 65 (inclusive) y tiene una enfermedad preexistente,
primero se intenta asignar una mesa para personas con enfermedad
preexistente y si no hay lugar disponible, se intenta asignar un turno
en la mesa para mayores.

-  Las mesas se identificarán con un número único y se asignará un presidente
de mesa, de quien se conoce el dni y el nombre
